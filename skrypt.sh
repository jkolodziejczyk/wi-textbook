#!/bin/bash
# Author: Joanna Kołodziejczyk

rm *.aux *.log
pdflatex main.tex
makeindex main.idx -s StyleInd.ist
biber main
pdflatex main.tex
pdflatex main.tex
